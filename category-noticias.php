<?php /* Template Name: Page News */ 
get_header();
?>


<section class="container__news container">
  <h1 class="titulo-seccion">Todas las noticias</h1>
  <?php
    
    echo '<div class="row">';   
    if ( have_posts() ):
      
      /* Start the Loop */
            while (have_posts() ) :
              
              the_post();
              /*
              * Include the Post-Format-specific template for the content.
              * If you want to override this in a child theme, then include a file
              * called content-___.php (where ___ is the Post Format name) and that will be used instead.
              */
              
        
                
             
             
                get_template_part( 'template-parts/page/content', 'news');
                
                

                
    
              
            endwhile;
            
         

      
        else :
          get_template_part( 'template-parts/content', 'none' );
        endif;
      echo'</div>';
    ?>
  <div class="paginador">
  <?php  echo paginate_links();?></div>
</section>

<?php get_footer(); ?>