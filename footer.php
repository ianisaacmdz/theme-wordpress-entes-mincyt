

	<footer id="colophon" class="site-footer" 404.php>
	<div class="row">	
		<div class="footer-gob">
		<h4>Gobierno Bolivariano</h4>
		<ul>		
			<li><a href="#">Entes 1 </a></li>
			<li><a href="#">Entes 2 </a></li>
			<li><a href="#">Entes 3 </a></li>
			<li><a href="#">Entes 4 </a></li>
		</ul>
	</div>

	<div class="footer-min">
		<h4>Ministerios</h4>
		<ul>		
			<li><a href="#">Ministerios 1 </a></li>
			<li><a href="#">Ministerios 2 </a></li>
			<li><a href="#">Ministerios 3 </a></li>
			<li><a href="#">Ministerios 4 </a></li>
		</ul>
	</div>
	
	<div class="footer-red">
		<h4>Redes Sociales</h4>
		<ul>		
			<li><a href="#" class="link"><i class="fab fa-facebook-f"></i> <span>FACEBOOK</span></a></li>
			<li><a href="#" class="link"><i class="fab fa-twitter"></i> <span>@Twitter</span></a></li>
			<li><a href="#" class="link"><i class="fab fa-instagram"></i><span>Instagram</span></a></li>
			<li><a href="#" class="link"><i class="fab fa-youtube"></i><span>YOUTUBE</span></a></li>
		</ul>
	</div>

	<div class="footer-hor">
		<h4>Horario de Atención</h4>
		<ul>		
			<li><h6>8:30am a 12:00pm <br> 1:00pm a 4:00pm</h6></li>
			<li><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3922.957367552371!2d-66.91390156001997!3d10.504024138163821!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2c924eccc469e0d2!2sMinisterio%20del%20Poder%20Popular%20para%20Ciencia%2C%20Tecnolog%C3%ADa%20e%20Innovaci%C3%B3n!5e0!3m2!1ses-419!2sve!4v1594406277602!5m2!1ses-419!2sve" width="200" height="100" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></li>
		</ul>
	</div>
	</div>


	<div class="footer-direction">
		<div class="row">
			<div>
				<h4>Adscrito:</h4>
				<p>Ministerio Del Poder Popular Para la Ciencia y Tecnologia</p>
			</div>
			<div>
				<h4>Dirección:</h4>
				<p>Ubicacion del ente adscrito al Mincyt con su telefonos numero de rif todos la informacion relacionada al lugar de ubicacion</p>
			</div>
			<div  class="topbutton">
				<button class="boton" onclick="scrollToTop(250,2)"><i class="fas fa-arrow-up"></i></button>
			</div>
		</div>
	</div>

	
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
