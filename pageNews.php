<?php /* Template Name: Page News */ 
get_header();
?>


<section class="container__news container">
  <h1 class="titulo-seccion">Todas las noticias</h1>
  <?php
    global $paged;
    echo '<div class="row">';   
    if ( have_posts() ):
      
      /* Start the Loop */
            while (  have_posts() ) :
              
              the_post();
              /*
              * Include the Post-Format-specific template for the content.
              * If you want to override this in a child theme, then include a file
              * called content-___.php (where ___ is the Post Format name) and that will be used instead.
              */
              
              $args = array(
                'post_type' => 'post',
                'paged'          => $paged,
                'orderby' => 'date'
              );
              $query = new WP_Query($args);
              
              while($query->have_posts()):
                  
                $query->the_post();
                
             
             
                get_template_part( 'template-parts/page/content', 'news');
                
                

                
              endwhile;
                echo '<div class="pagination">';  
                    echo '<div class="pagination__container">';    
                      echo paginate_links( array(
                        
                        'format' => '?paged=%#%',
                        'current' => $paged,
                        'total'   => $query->max_num_pages,
                        'prev_text'          => __( '<i class="fas fa-angle-left left"></i>' ),
                        'next_text'          => __( '<i class="fas fa-angle-right right"></i>' ),
                      ) );
                      echo '</div>';   
                echo '</div>';
            endwhile;
            

      
        else :
          get_template_part( 'template-parts/content', 'none' );
        endif;
      echo'</div>';
    ?>
  
</section>

<?php get_footer(); ?>