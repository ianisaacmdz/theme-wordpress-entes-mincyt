<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Codecyt
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'codecyt' ); ?></a>

	<header id="masthead" class="site-header">
	<?php get_template_part( 'template-parts/home/content', 'cintillo' );?>
	<div class="menu-container">
		
		<div class="row">
		<?php
			the_custom_logo();
			?>
	
		<nav id="site-navigation" class="main-navigation">
			<div class="menu-toggle">
				<i class="fas fa-bars"></i>
			</div>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'container_id'   => 'menu-list'
				)
			);
			?>
		</nav><!-- #site-navigation -->

		</div>
		
	</div>
	

	<!--	<div class="site-branding">

			<!- <?php
			// if ( is_front_page() && is_home() ) :
			// 	?>
			// 	<h1 class="site-title"><a href="<?php// echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php// bloginfo( 'name' ); ?></a></h1>
			// 	<?php
			// else :
			// 	?>
			// 	<p class="site-title"><a href="<?php// echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php// bloginfo( 'name' ); ?></a></p>
			// 	<?php
			// endif;
			// $codecyt_description = get_bloginfo( 'description', 'display' );
			// if ( $codecyt_description || is_customize_preview() ) :
			// 	?>
			// 	<p class="site-description"><?php// echo $codecyt_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php// endif; ?> 
		</div> .site-branding -->

	
	</header><!-- #masthead -->
