<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Mincyt Themes Entes
 * @subpackage ThemesEntes
 * @since Mincyt Themes Entes 1.0
 * @version 0.1
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("news"); ?>>
    <a class="news__link" href="<?php the_permalink(); ?>">
    
        <figure class="news__thumbnail">
                <?php codecyt_post_thumbnail(); ?>
        
        </figure>
    
        <div class="news__contents">
            <div class="news__title">
                <?php the_title( '<h2>', '</h2>' ); ?>
            </div>
            
            <div class="news__extra">
                <?php the_excerpt(); ?>
            </div>
            <div class="news__dates">
                <?php  codecyt_posted_on(); ?>
            </div>    
        </div>

    </a>
</article><!-- #post-<?php the_ID(); ?> -->

