<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Mincyt Themes Entes
 * @subpackage ThemesEntes
 * @since Mincyt Themes Entes 1.0
 * @version 0.1
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("entry"); ?>>
	<div class="entry__news">

        <figure class="entry__thumbnail">
            <a href="<?php the_permalink(); ?>">
                <?php codecyt_post_thumbnail(); ?>
            </a>
        </figure>

        <div class="entry__contents">
            

            <div class="entry__title">
                <?php the_title( '<h2>', '</h2>' ); ?>
            </div>
            <div class="entry__dates">
                <?php codecyt_posted_by(); echo " | "; codecyt_posted_on(); ?>
            </div>
            

            <a class="entry__button" href="<?php the_permalink(); ?>" rel="bookmark">
                Leer Más
            </a>
        </div>

    </div>
</article><!-- #post-<?php the_ID(); ?> -->

