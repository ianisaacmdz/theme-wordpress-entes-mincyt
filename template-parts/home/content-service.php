<div class="services__container-item row">
    <a href="#" class="service">
        <figure class="service__icon">
            <i class="fas fa-bullhorn"></i>
        </figure>
        <div class="service__title">
            <h3>CURSOS</h3>
        </div>
        <div class="service__description">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
            Sapiente atque quae laudantium reiciendis, 
        </div>
        
    </a>
    <a href="#" class="service">
        <figure class="service__icon">
            <i class="fas fa-database"></i>
        </figure>
        <div class="service__title">
            <h3>REPOSITORIO</h3>
        </div>
        <div class="service__description">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
            Sapiente atque quae laudantium reiciendis, 
        </div>
        
    </a>
    <a href="#" class="service">
        <figure class="service__icon">
            <i class="fas fa-money-bill-alt"></i>
        </figure>
        <div class="service__title">
            <h3>BALANCES</h3>
        </div>
        <div class="service__description">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
            Sapiente atque quae laudantium reiciendis, 
        </div>
        
    </a>
    <a href="#" class="service">
        <figure class="service__icon">
            <i class="fas fa-cloud"></i>
        </figure>
        <div class="service__title">
            <h3>NUBE</h3>
        </div>
        <div class="service__description">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
            Sapiente atque quae laudantium reiciendis, 
        </div>
        
    </a>
    <a href="#" class="service">
        <figure class="service__icon">
        <i class="fas fa-envelope"></i>
        </figure>
        <div class="service__title">
            <h3>CORREO</h3>
        </div>
        <div class="service__description">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
            Sapiente atque quae laudantium reiciendis, 
        </div>
        
    </a>
    <a href="#" class="service">
        <figure class="service__icon">
            <i class="fas fa-network-wired"></i>
        </figure>
        <div class="service__title">
            <h3>INTRANET</h3>
        </div>
        <div class="service__description">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. 
            Sapiente atque quae laudantium reiciendis, 
        </div>
        
    </a>
</div>