<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Codecyt
 */

get_header();

// $text = get_theme_mod('campo_texto');

?>

	
<main id="primary" class="">
	<?php echo do_shortcode('[metaslider id="17"]'); ?>
		
		
		<section id="services" class="container"><!-- #sección de servición -->
			<div class="row services__container">
				<h2 class="titulo-seccion">SERVICIOS</h2>
				<?php get_template_part( 'template-parts/home/content', 'service' ); ?>
			</div>
		</section>

		<section id="noticas" class="container"><!-- #sección de noticas -->
			<h2 class="titulo-seccion">NOTICIAS</h2>
			<div class="row noticas__container">
				<?php get_template_part( 'template-parts/home/content', 'news' ); ?>
			
			</div>
		</section>
		<?php get_template_part( 'template-parts/home/content', 'programas' );?>
		<?php get_template_part( 'template-parts/home/content', 'entes' );?>
	</main><!-- #main -->



	
		

<?php
//get_sidebar();
get_footer();
